import Dashboard from "./pages/Dashboard";
import Login from "./pages/Login";
import Profile from "./pages/Profile";
import Users from "./pages/Users";
import SignUp from "./pages/SignUp";
import { createBrowserRouter, RouterProvider } from "react-router-dom";

function App() {
  const router = createBrowserRouter([
    {
      path: "/",
      element: (
        <Dashboard />
      ),
    },
    {
      path: "/login",
      element: (
        <Login />
      ),
    },
    {
      path: "/sign-up",
      element: (
        <SignUp />
      ),
    },
    {
      path: "/profile",
      element: (
        <Profile />
      ),
    },
    {
      path: "/users",
      element: (
        <Users />
      ),
    },
  ]);

  return <RouterProvider router={router} />;
}

export default App;
