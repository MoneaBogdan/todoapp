function EditUserButton({ setIsOpen }) {
    return (
        <button
          type="button"
          className="text-white inline-flex items-center bg-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800 ml-2"
          onClick={setIsOpen}
        >
          Edit User
        </button>
    );
};

export default EditUserButton;