import React, { useState } from "react";
import { TASK_STATUS } from "../../enums";
import { useUserSearch } from "../../hooks/useUserSearch";

function AddTaskModal({ open, onClose, onSubmit }) {
  const [task, setTask] = useState({
    title: "",
    status: TASK_STATUS.TODO,
    description: "",
    userId: 0,
  });

  const {
    searchQuery,
    searchResults,
    handleSearchQueryChange,
    handleSelectedUser,
  } = useUserSearch();

  const handleUserSelect = (user) => {
    setTask({ ...task, userId: user.id });
    handleSelectedUser(user);
  };

  const handleOnClick = (e) => {
    e.preventDefault();
    onSubmit(task);

    setTask({
      title: "",
      status: TASK_STATUS.TODO,
      description: "",
      userId: 0,
    });

    onClose();
  };

  return (
    <>
      {open && (
        <div
          id="defaultModal"
          className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto overflow-x-hidden"
        >
          <div className="relative p-4 w-full max-w-2xl h-full md:h-auto">
            <div className="relative p-4 bg-white rounded-lg shadow dark:bg-gray-800 sm:p-5">
              <div className="flex justify-between items-center pb-4 mb-4 rounded-t border-b sm:mb-5 dark:border-gray-600">
                <h3 className="text-lg font-semibold text-gray-900 dark:text-white">
                  Add Task
                </h3>
                <button
                  type="button"
                  className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white"
                  onClick={onClose}
                >
                  <svg
                    aria-hidden="true"
                    className="w-5 h-5"
                    fill="currentColor"
                    viewBox="0 0 20 20"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      fillRule="evenodd"
                      d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                      clipRule="evenodd"
                    ></path>
                  </svg>
                  <span className="sr-only">Close modal</span>
                </button>
              </div>
              <form onSubmit={handleOnClick} action="#">
                <div className="grid gap-4 mb-4 sm:grid-cols-2">
                  <div>
                    <label
                      htmlFor="title"
                      className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                    >
                      Title
                    </label>
                    <input
                      type="text"
                      id="title"
                      className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-600 focus:border-blue-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      placeholder="Title of the task"
                      value={task.title}
                      onChange={(evt) =>
                        setTask({ ...task, title: evt.target.value })
                      }
                      required
                    />
                  </div>
                  <div>
                    <label
                      htmlFor="status"
                      className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                    >
                      Status
                    </label>
                    <select
                      id="status"
                      className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      placeholder="Title of the task"
                      value={task.status}
                      onChange={(evt) =>
                        setTask({ ...task, status: evt.target.value })
                      }
                    >
                      <option value={TASK_STATUS.TODO}>To do</option>
                      <option value={TASK_STATUS.IN_PROGRESS}>
                        In Progress
                      </option>
                      <option value={TASK_STATUS.COMPLETED}>Completed</option>
                    </select>
                  </div>
                  <div className="sm:col-span-2">
                    <label
                      htmlFor="description"
                      className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                    >
                      Description
                    </label>
                    <textarea
                      id="description"
                      rows="4"
                      className="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      placeholder="Write task description here"
                      value={task.description}
                      onChange={(evt) =>
                        setTask({ ...task, description: evt.target.value })
                      }
                      required
                    ></textarea>
                  </div>
                  <div>
                    <label
                      htmlFor="assignee"
                      className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                    >
                      Assignee
                    </label>
                    <input
                      type="text"
                      id="assignee"
                      className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-600 focus:border-blue-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      placeholder="Search for assignee"
                      value={searchQuery}
                      onChange={(evt) =>
                        handleSearchQueryChange(evt.target.value)
                      }
                    />
                    <ul className="mt-2">
                      {searchResults.map((user) => (
                        <li
                          key={user.id}
                          className="cursor-pointer p-2 hover:bg-gray-200"
                          onClick={() => {
                            handleUserSelect(user);
                          }}
                        >
                          {user.firstName} {user.lastName}
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>
                <div className="flex justify-end">
                  <button
                    type="submit"
                    className="text-white inline-flex items-center bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                  >
                    <svg
                      className="mr-1 -ml-1 w-6 h-6"
                      fill="currentColor"
                      viewBox="0 0 20 20"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fillRule="evenodd"
                        d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z"
                        clipRule="evenodd"
                      ></path>
                    </svg>
                    Add new task
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default AddTaskModal;
