import { useState, useEffect } from "react";
import { useApi } from "../../api/useApi";
import { TASK_STATUS_COLOR } from "../../enums";
import EditTaskModal from "./EditTaskModal";

function Task({ task, editTask, deleteTask }) {
  const { getOneData, getAvatar } = useApi("/users");
  const [userName, setUserName] = useState("");
  const [userAvatar, setUserAvatar] = useState("");

  useEffect(() => {
    getOneData(task.userId).then((user) => {
      setUserName(`${user.firstName} ${user.lastName}`);
      if (user.avatarUrl) {
        getAvatar(user.avatarUrl).then((avatar) => {
          setUserAvatar(avatar);
        });
      }
    });
  }, []);

  return (
    <>
      <EditTaskModal
        task={task}
        modalId={`modal${task.id}`}
        editTask={editTask}
        deleteTask={deleteTask}
      />
      <div
        className={`p-4 bg-${
          TASK_STATUS_COLOR[task.status]
        }-200 rounded cursor-pointer`}
        data-modal-target={`modal${task.id}`}
        data-modal-toggle={`modal${task.id}`}
      >
        <h2
          className={`text-xl font-bold text-${
            TASK_STATUS_COLOR[task.status]
          }-800 dark:text-${TASK_STATUS_COLOR[task.status]}-800 mb-2`}
        >
          {task.title}
        </h2>
        <p
          className={`text-${TASK_STATUS_COLOR[task.status]}-700 dark:text-${
            TASK_STATUS_COLOR[task.status]
          }-600`}
        >
          {task.description}
        </p>
        <div className="flex items-center mt-2">
          {userAvatar ? (
            <img
              src={userAvatar}
              alt={`${userName}'s avatar`}
              className="w-6 h-6 rounded-full mr-2"
            />
          ) : (
            <p
              className={`font-bold text-${
                TASK_STATUS_COLOR[task.status]
              }-700 dark:text-${TASK_STATUS_COLOR[task.status]}-600`}
            >
              Assignee: {userName}
            </p>
          )}
        </div>
      </div>
    </>
  );
}

export default Task;
