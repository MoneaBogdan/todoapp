import React from "react";

// Deci practic ca sa aiba fiecare treaba lui, butonu e buton, modalu e modal
// butonu deschide modalu\
// o sa vezi ca ne ajuta si la testing
function CreateTaskButton({ onCreateTaskHandler }) {
  return (
    <div className="flex justify-center mb-3">
      <button
        id="defaultModalButton"
        className="block text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
        type="button"
        onClick={onCreateTaskHandler}
      >
        Create Task
      </button>
    </div>
  );
}

export default CreateTaskButton;
