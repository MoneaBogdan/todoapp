import { useState, useEffect } from "react";
import Layout from "../components/shared/Layout";
import AddUserModal from "../components/users/AddUserModal";
import { useApi } from "../api/useApi";
import EditUserModal from "../components/users/EditUserModal";
import DeleteUserButton from "../components/users/DeleteUserButton";
import EditUserButton from "../components/users/EditUserButton";

function Users() {
  const {
    data,
    addDataWithAvatar,
    updateDataWithAvatar,
    deleteData,
    getAvatar,
  } = useApi("/users");
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedUserId, setSelectedUserId] = useState(null);
  const [avatars, setAvatars] = useState({});

  useEffect(() => {
    const fetchAvatars = async () => {
      const avatarsData = {};
      for (const user of data) {
        if (user.avatarUrl) {
          const avatar = await getAvatar(user.avatarUrl);
          avatarsData[user.id] = avatar;
        }
      }
      setAvatars(avatarsData);
    };

    fetchAvatars();
  }, [data]);

  return (
    <Layout>
      <AddUserModal addUser={addDataWithAvatar} />
      <div className="relative overflow-x-auto">
        <table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
              <th scope="col" className="px-6 py-3">
                Avatar
              </th>
              <th scope="col" className="px-6 py-3">
                First Name
              </th>
              <th scope="col" className="px-6 py-3">
                Last Name
              </th>
              <th scope="col" className="px-6 py-3">
                Email
              </th>
              <th scope="col" className="px-6 py-3">
                Age
              </th>
            </tr>
          </thead>
          <tbody>
            {data.map((user) => (
              <tr
                key={user.id}
                className="bg-white border-b dark:bg-gray-800 dark:border-gray-700"
              >
                <td className="px-6 py-4">
                  {avatars[user.id] ? (
                    <img
                      src={avatars[user.id]}
                      alt={`${user.firstName} ${user.lastName}'s avatar`}
                      className="w-6 h-6 mb-1 ml-1 mt-1 rounded-full"
                    />
                  ) : (
                    <div className="w-6 h-6 mb-1 ml-1 mt-1 rounded-full bg-gray-300"></div>
                  )}
                </td>
                <td className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                  {user.firstName}
                </td>
                <td className="px-6 py-4">{user.lastName}</td>
                <td className="px-6 py-4">{user.email}</td>
                <td className="px-6 py-4">{user.age}</td>
                <td>
                  <EditUserButton
                    setIsOpen={() => {
                      setSelectedUserId(user.id);
                      setIsModalOpen(true);
                    }}
                  />
                </td>
                <td>
                  <DeleteUserButton
                    onDeleteHandler={() => {
                      deleteData(user.id);
                    }}
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      {isModalOpen && (
        <EditUserModal
          userId={selectedUserId}
          open={isModalOpen}
          onClose={() => {
            setIsModalOpen(false);
            setSelectedUserId("");
          }}
          onSubmit={(user) => {
            setIsModalOpen(false);
            updateDataWithAvatar(user);
          }}
        />
      )}
    </Layout>
  );
}

export default Users;
