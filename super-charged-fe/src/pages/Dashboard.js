import Layout from "../components/shared/Layout";
import TaskList from "../components/tasks/TaskList";
import AddTaskModal from "../components/tasks/AddTaskModal";
import { useApi } from "../api/useApi";
import { TASK_STATUS } from "../enums";
import CreateTaskButton from "../components/tasks/CreateTaskButton";
import { useState } from "react";

function Dashboard() {
  const { data, addData, updateData, deleteData } = useApi("/tasks");
  const [openAddTaskModal, setOpenAddTaskModal] = useState(false);

  const taskStatus = [
    TASK_STATUS.TODO,
    TASK_STATUS.IN_PROGRESS,
    TASK_STATUS.COMPLETED,
  ];

  return (
    <>
      <Layout>
        <CreateTaskButton
          onCreateTaskHandler={() => {
            setOpenAddTaskModal(true);
          }}
        />
        <AddTaskModal
          open={openAddTaskModal}
          onClose={() => {
            setOpenAddTaskModal(false);
          }}
          onSubmit={(task) => {
            addData(task);
          }}
        />
        <div className="grid grid-cols-1 md:grid-cols-3 gap-6">
          {taskStatus.map((status) => (
            <TaskList
              status={status}
              tasks={data.filter((task) => task.status === status)}
              editTask={updateData}
              deleteTask={deleteData}
            />
          ))}
        </div>
      </Layout>
    </>
  );
}

export default Dashboard;
