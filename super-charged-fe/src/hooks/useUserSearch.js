import { useState, useEffect } from "react";
import { useApi } from "../api/useApi";
import { useDebounceCallback } from "../hooks/useDebounceCallback";

export const useUserSearch = () => {
  const { getData } = useApi("/users");
  const [searchQuery, setSearchQuery] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const debouncedSearchQuery = useDebounceCallback(searchQuery, 300);

  useEffect(() => {
    if (debouncedSearchQuery) {
      getData({ search: debouncedSearchQuery }).then((results) => {
        setSearchResults(results);
      });
    } else {
      setSearchResults([]);
    }
  }, [debouncedSearchQuery]);

  const handleSearchQueryChange = (query) => {
    setSearchQuery(query);
  };

  const handleSelectedUser = (user) => {
    setSearchQuery(`${user.firstName} ${user.lastName}`);
    setSearchResults([]);
  };

  return {
    searchQuery,
    searchResults,
    handleSearchQueryChange,
    handleSelectedUser,
  };
};
