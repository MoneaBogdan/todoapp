import { useState, useEffect } from "react";

const API_URL = process.env.REACT_APP_API_KEY;

export const useApi = (router) => {
  const [datas, setData] = useState([]);

  const getData = async (params = {}) => {
    // Construct query string from params
    const queryString = new URLSearchParams(
      Object.entries(params).reduce((acc, [key, value]) => {
        if (value !== null && value !== undefined) {
          acc[key] = value;
        }
        return acc;
      }, {})
    ).toString();

    const url = `${API_URL}${router}${queryString ? `?${queryString}` : ""}`;

    try {
      const response = await fetch(url, {
        method: "GET",
      });
      const data = await response.json();
      setData(data);
      return data;
    } catch (error) {
      console.error("Error while fetching data: ", error);
    }
  };

  const getOneData = async (id) => {
    const response = await fetch(`${API_URL}${router}/${id}`, {
      method: "GET",
    }).then((data) => data.json());

    return response;
  };

  const addData = async (newData) => {
    const data = { ...newData, id: datas.length + 1 };

    await fetch(API_URL + router, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .catch((error) =>
        console.error("Error while adding a new data: ", error)
      );

    getData();
  };

  // This method is specific for users only so i will hardcode /users
  // instead of router
  const addDataWithAvatar = async (newData) => {
    const formData = new FormData();
    formData.append("firstName", newData.firstName);
    formData.append("lastName", newData.lastName);
    formData.append("age", Number(newData.age));
    formData.append("email", newData.email);
    formData.append("avatarUrl", newData.avatarUrl);

    await fetch(API_URL + "/users", {
      method: "POST",
      body: formData,
    })
      .then((response) => response.json())
      .catch((error) =>
        console.error("Error while adding a new data: ", error)
      );

    getData();
  };

  const updateData = async (updatedData) => {
    await fetch(API_URL + router + "/" + updatedData.id, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(updatedData),
    })
      .then((response) => response.json())
      .catch((error) => console.error("Error while updating data: ", error));

    getData();
  };

  const updateDataWithAvatar = async (updatedData) => {
    const formData = new FormData();
    formData.append("firstName", updatedData.firstName);
    formData.append("lastName", updatedData.lastName);
    formData.append("age", Number(updatedData.age));
    formData.append("email", updatedData.email);
    formData.append("avatarUrl", updatedData.avatarUrl);

    await fetch(API_URL + "/users/" + updatedData.id, {
      method: "PUT",
      body: formData,
    })
      .then((response) => response.json())
      .catch((error) => console.error("Error while updating data: ", error));

    getData();
  };

  const deleteData = async (deletedDataId) => {
    await fetch(API_URL + router + "/" + deletedDataId, {
      method: "DELETE",
    }).catch((error) => console.error("Error while deleting data: ", error));

    getData();
  };

  const getAvatar = async (urlToImage) => {
    const pathToImage = {
      url: urlToImage,
    };
    const blob = await fetch(`${API_URL}/users/avatar`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(pathToImage),
    }).then((response) => response.blob());
    return URL.createObjectURL(blob);
  };

  useEffect(() => {
    getData();
  }, []);

  return {
    data: datas,
    getData,
    addData,
    addDataWithAvatar,
    updateData,
    updateDataWithAvatar,
    deleteData,
    getOneData,
    getAvatar,
  };
};
