export const TASK_STATUS_COLOR = {
  TODO: "pink",
  IN_PROGRESS: "blue",
  COMPLETED: "green",
};

export const TASK_STATUS = {
  TODO: "TODO",
  IN_PROGRESS: "IN_PROGRESS",
  COMPLETED: "COMPLETED",
};

Object.freeze(TASK_STATUS_COLOR);
