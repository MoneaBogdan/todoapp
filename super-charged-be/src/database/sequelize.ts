import { Sequelize, Dialect } from 'sequelize';
import dotenv from 'dotenv';

dotenv.config();

// Load environment variables
const dialect = process.env.DB_DIALECT as Dialect;
const storage = process.env.DB_STORAGE;

if (!dialect || !storage) {
  throw new Error(
    'Environment variables DB_DIALECT and DB_STORAGE must be set.',
  );
}

// Initialize Sequelize with SQLite
const sequelize = new Sequelize({
  dialect,
  storage,
  dialectOptions: {
    foreignKeys: {
      allowNull: false,
    },
  },
});

export default sequelize;
