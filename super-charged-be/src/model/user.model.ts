import { DataTypes, Model } from 'sequelize';
import sequelize from '@database/sequelize';
import TaskModel from './task.model';

class UserModel extends Model {
  public id!: number;
  public firstName!: string;
  public lastName!: string;
  public age!: number;
  public email!: string;
  public avatarUrl!: string;
}

UserModel.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    firstName: {
      type: DataTypes.STRING(128),
      allowNull: false,
    },
    lastName: {
      type: DataTypes.STRING(128),
      allowNull: false,
    },
    age: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING(128),
      allowNull: false,
      validate: {
        isEmail: true,
      },
    },
    avatarUrl: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
  },
  {
    sequelize,
    tableName: 'Users',
  },
);

TaskModel.belongsTo(UserModel, { foreignKey: 'userId', onDelete: 'CASCADE' });
UserModel.hasMany(TaskModel, { foreignKey: 'userId', onDelete: 'CASCADE' });

export default UserModel;
