import { TaskStatus } from '@tasks/task.enums';
import { DataTypes, Model } from 'sequelize';
import sequelize from '@database/sequelize';
import UserModel from './user.model';

class TaskModel extends Model {
  public id!: number;
  public title!: string;
  public description!: string;
  public status!: TaskStatus;
}

TaskModel.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    status: {
      type: DataTypes.ENUM(...Object.values(TaskStatus)),
      allowNull: false,
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  },
  {
    sequelize,
    modelName: 'Tasks',
  },
);

// TaskModel.belongsTo(UserModel, { foreignKey: 'userId', onDelete: 'CASCADE' });

export default TaskModel;
