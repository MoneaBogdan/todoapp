import { Request, Response } from 'express';
import { body, param, validationResult } from 'express-validator';
import * as tasksService from './task.service';
import * as statusCodes from '@utils/statusCodes';

// Middleware for validating request data
const validateRequest = (req: Request, res: Response, next: Function) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(statusCodes.BAD_REQUEST).json({ errors: errors.array() });
  }
  next();
};

// Utility function to serialize the task entity
const serializeTask = (body: any) => {
  return {
    title: body.title,
    description: body.description,
    status: body.status,
    userId: body.userId,
  };
};

// Function to retrieve all tasks
export const getTasks = async (req: Request, res: Response) => {
  const tasks = await tasksService.getAllTasks();
  res.status(statusCodes.OK).json(tasks);
};

// Function to create a new task
export const createTask = [
  body('title').notEmpty().withMessage('Title is required'),
  body('description').notEmpty().withMessage('Description is required'),
  body('status').notEmpty().withMessage('Status is required'),
  validateRequest,
  async (req: Request, res: Response) => {
    const newTask = serializeTask(req.body);
    const createdTask = await tasksService.createTask(newTask);
    res.status(statusCodes.CREATED).json(createdTask);
  },
];

// Function to update an existing task
export const updateTask = [
  param('id').isInt().withMessage('Task ID must be an integer'),
  body('description').notEmpty().withMessage('Description is required'),
  body('status').notEmpty().withMessage('Status is required'),
  validateRequest,
  async (req: Request, res: Response) => {
    const { id } = req.params;
    const updatedTask = serializeTask(req.body);
    const result = await tasksService.updateTask(Number(id), updatedTask);

    if (result === null) {
      return res
        .status(statusCodes.NOT_FOUND)
        .json({ message: 'Task not found!' });
    }
    res.status(statusCodes.OK).json(result);
  },
];

// Function to delete an existing task
export const deleteTask = [
  param('id').isInt().withMessage('Task ID must be an integer'),
  validateRequest,
  async (req: Request, res: Response) => {
    const { id } = req.params;
    const deletedTask = await tasksService.deleteTask(Number(id));

    if (deletedTask === null) {
      return res
        .status(statusCodes.NOT_FOUND)
        .json({ message: 'Task not found!' });
    }
    res.status(statusCodes.OK).json({ message: 'Task deleted successfully !' });
  },
];
