import { Task } from './task.interfaces';
import TaskModel from '../model/task.model';

// Function to get all tasks
export const getAllTasks = async (): Promise<Array<TaskModel>> => {
  try {
    const tasks = await TaskModel.findAll();
    return tasks;
  } catch (error) {
    console.error('Error getting all tasks: ', error);
    throw error;
  }
};

// Function to create a new task
export const createTask = async (task: Task): Promise<TaskModel> => {
  try {
    const { title, status, description, userId } = task;
    return await TaskModel.create({ title, status, description, userId });
  } catch (error) {
    console.error('Error creating new task: ', error);
    throw error;
  }
};

// Function to update an existing task
export const updateTask = async (
  id: number,
  updatedTask: Task,
): Promise<TaskModel | null> => {
  try {
    const task = await TaskModel.findByPk(id);

    if (task) {
      Object.assign(task, updatedTask);

      await task.save();
      return task;
    } else {
      throw new Error('Task not found');
    }
  } catch (error) {
    console.error('Error updating task: ', error);
    throw error;
  }
};

// Function to delete an existing task
export const deleteTask = async (id: number) => {
  try {
    const task = await TaskModel.findByPk(id);
    if (task) {
      await task.destroy();
      return { message: 'Task deleted!' };
    } else {
      throw new Error('Task not found!');
    }
  } catch (error) {
    console.error('Error deleting task: ', error);
    throw error;
  }
};
