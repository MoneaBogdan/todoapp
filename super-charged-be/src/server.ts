import dotenv from 'dotenv';
import express from 'express';
import morgan from 'morgan';
import taskRoutes from '@tasks/task.routes';
import userRoutes from './users/user.routes';
import cors from 'cors';
import { errorHandler } from '@middlewares/errorHandler';
import sequelize from '@database/sequelize';
import path from 'path';

dotenv.config();

// Create Express server
const app = express();

// Serve avatar images
app.use(
  'public/images/avatars',
  express.static(path.join(__dirname, 'public/images/avatars')),
);

// Enable CORS
app.use(cors());

// Use the express.json() middleware to parse JSON request bodies
app.use(express.json());

// Use the morgan middleware to log HTTP requests
app.use(morgan('dev'));

app.use('/api/tasks', taskRoutes);
app.use('/api/users', userRoutes);

app.use(errorHandler);

const PORT = process.env.PORT || 3001;

app.listen(PORT, async () => {
  console.log(`Server is running on port ${PORT}`);
  try {
    await sequelize.sync();
    console.log('Database synced!');
  } catch (error) {
    console.log('Failed to sync database:', error);
  }
});
