import { Request, Response } from 'express';
import { body, param, query, validationResult } from 'express-validator';
import * as userService from './user.service';
import * as statusCodes from '@utils/statusCodes';
import { Op } from 'sequelize';
import multer from 'multer';
import path from 'path';

// Middleware for validating request data
const validateRequest = (req: Request, res: Response, next: Function) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(statusCodes.BAD_REQUEST).json({ errors: errors.array() });
  }
  next();
};

// Utility function to serialize the user entity
const serializeUser = (body: any) => {
  return {
    firstName: body.firstName,
    lastName: body.lastName,
    age: body.age,
    email: body.email,
    avatarUrl: body.avatarUrl,
  };
};

// Setup multer for image uploads
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'public/images/avatars');
  },
  filename: (req, file, cb) => {
    cb(null, `${file.originalname}`);
  },
});

const upload = multer({
  storage,
  limits: { fileSize: 1024 * 1024 }, // Restrict size to 1MB
});

// Function to retrieve all users with search and pagination
export const getUsers = [
  query('search').optional().isString(),
  query('page').optional().isInt({ min: 1 }),
  query('size').optional().isInt({ min: 1, max: 100 }),
  validateRequest,
  async (req: Request, res: Response) => {
    const { search, page = 1, size = 10 } = req.query;
    const queryOptions: any = {
      limit: size,
      offset: (Number(page) - 1) * Number(size),
    };

    if (search) {
      queryOptions.where = {
        [Op.or]: [
          { firstName: { [Op.like]: `%${search}%` } },
          { lastName: { [Op.like]: `%${search}%` } },
          { email: { [Op.like]: `%${search}%` } },
        ],
      };
    }

    const users = await userService.getAllUsers(queryOptions);
    res.status(statusCodes.OK).json(users);
  },
];

// Function to create a new user
export const createUser = [
  upload.single('avatarUrl'),
  body('firstName').notEmpty().withMessage('First name is required'),
  body('lastName').notEmpty().withMessage('Last name is required'),
  body('age').isInt({ min: 0 }).withMessage('Age must be a positive integer'),
  body('email').isEmail().withMessage('Invalid email'),
  validateRequest,
  async (req: Request, res: Response) => {
    const newUser = serializeUser(req.body);
    newUser.avatarUrl = req.file?.path;
    const createdUser = await userService.createUser(newUser);
    res.status(statusCodes.CREATED).json(createdUser);
  },
];

// Function to update an existing user
export const updateUser = [
  upload.single('avatarUrl'),
  param('id').isInt().withMessage('User ID must be an integer'),
  body('firstName').notEmpty().withMessage('First name is required'),
  body('lastName').notEmpty().withMessage('Last name is required'),
  body('age').isInt({ min: 0 }).withMessage('Age must be a positive integer'),
  body('email').isEmail().withMessage('Invalid email'),
  validateRequest,
  async (req: Request, res: Response) => {
    const { id } = req.params;
    const updatedUser = serializeUser(req.body);
    if (req.file) {
      updatedUser.avatarUrl = req.file.path;
    }
    const result = await userService.updateUser(Number(id), updatedUser);

    if (result === null) {
      return res
        .status(statusCodes.NOT_FOUND)
        .json({ message: 'User not found!' });
    }
    res.status(statusCodes.OK).json(result);
  },
];

// Function to delete an existing user
export const deleteUser = [
  param('id').isInt().withMessage('Task ID must be an integer'),
  validateRequest,
  async (req: Request, res: Response) => {
    const { id } = req.params;
    const deletedUser = await userService.deleteUser(Number(id));

    if (deletedUser === null) {
      return res
        .status(statusCodes.NOT_FOUND)
        .json({ message: 'User not found!' });
    }
    res.status(statusCodes.OK).json({ message: 'User deleted successfully !' });
  },
];

export const getOneUser = [
  async (req: Request, res: Response) => {
    try {
      const { id } = req.params;
      const user = await userService.getOneUser(+id);
      res.status(statusCodes.OK).json(user);
    } catch (error) {
      console.error('Error while retrieving users: ', error);
      res
        .status(statusCodes.INTERNAL_SERVER_ERROR)
        .json({ message: 'Internal Server Error' });
    }
  },
];

export const getAvatar = [
  async (req: Request, res: Response) => {
    const { url } = req.body;
    const imagePath = path.join(__dirname, `../../${url}`);
    res.sendFile(imagePath);
  },
];
