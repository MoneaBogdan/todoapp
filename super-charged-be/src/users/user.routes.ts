import { Router } from 'express';
import {
  getUsers,
  createUser,
  updateUser,
  deleteUser,
  getOneUser,
  getAvatar,
} from './user.controller';

// New Router instance
const router = Router();

router.get('/', getUsers);
router.post('/', createUser);
router.put('/:id', updateUser);
router.delete('/:id', deleteUser);
router.get('/:id', getOneUser);
router.post('/avatar', getAvatar);

export default router;
