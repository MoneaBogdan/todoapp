import { User } from './user.interfaces';
import UserModel from '../model/user.model';

// Function to get all users
export const getAllUsers = async (
  queryOptions: any,
): Promise<Array<UserModel>> => {
  try {
    const users = await UserModel.findAll(queryOptions);
    return users;
  } catch (error) {
    console.error('Error while fetching all users: ', error);
    throw error;
  }
};

// Function to create an user
export const createUser = async (user: User): Promise<UserModel> => {
  try {
    const { firstName, lastName, age, email, avatarUrl } = user;
    return await UserModel.create({
      firstName,
      lastName,
      age,
      email,
      avatarUrl,
    });
  } catch (error) {
    console.error('Error while creating a new user: ', error);
    throw error;
  }
};

// Function to update an existing user
export const updateUser = async (
  id: number,
  updatedUser: User,
): Promise<UserModel | null> => {
  try {
    const user = await UserModel.findByPk(id);

    if (user) {
      Object.assign(user, updatedUser);
      await user.save();
      return user;
    } else {
      throw new Error('User not found!');
    }
  } catch (error) {
    console.error('Error while updating user: ', error);
    throw error;
  }
};

// Function to delete an existing user
export const deleteUser = async (id: number) => {
  try {
    const user = await UserModel.findByPk(id);

    if (user) {
      await user.destroy();
      return { message: 'User deleted' };
    } else {
      throw new Error('User not found');
    }
  } catch (error) {
    console.error('Error while deleting user: ', error);
    throw error;
  }
};

// Function to get user by id
export const getOneUser = async (id: number) => {
  try {
    return await UserModel.findByPk(id);
  } catch (error) {
    console.error('Error while fetching all users: ', error);
    throw error;
  }
};
